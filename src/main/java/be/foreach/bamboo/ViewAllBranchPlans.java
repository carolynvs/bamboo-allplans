package be.foreach.bamboo;

import com.atlassian.bamboo.build.CookieCutter;
import com.atlassian.bamboo.deployments.cache.LinkedDeploymentProjectCacheService;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanFavouriteService;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import org.acegisecurity.acls.Permission;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ViewAllBranchPlans extends BambooActionSupport {

    private final CachedPlanManager cachedPlanManager;
    private final ProjectManager projectManager;
    private final PlanFavouriteService planFavouriteService;
    private final List<String> branchNames = new ArrayList<String>();
    private String currentlyFilteredBranch;

    private Map<Project, Map<ImmutablePlan, List<ImmutablePlan>>> allBranchPlans = new LinkedHashMap<Project, Map<ImmutablePlan, List<ImmutablePlan>>>();

    public ViewAllBranchPlans(ProjectManager projectManager, BambooAuthenticationContext bambooAuthenticationContext, CookieCutter cookieCutter, PlanFavouriteService planFavouriteService, BambooPermissionManager bambooPermissionManager,
                              LinkedDeploymentProjectCacheService linkedDeploymentProjectCacheService, PlanExecutionManager planExecutionManager, CachedPlanManager cachedPlanManager) {
        this.projectManager = projectManager;
        this.planFavouriteService = planFavouriteService;
        this.cachedPlanManager = cachedPlanManager;
        this.setCookieCutter( cookieCutter );
        this.setAuthenticationContext( bambooAuthenticationContext );
        this.setBambooPermissionManager( bambooPermissionManager );
        this.setLinkedDeploymentProjectCacheService( linkedDeploymentProjectCacheService );
        this.setPlanExecutionManager( planExecutionManager );
    }

    @Override
    public String doDefault() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        currentlyFilteredBranch  = request.getParameter( "filter" );
        List<Project> projects =  projectManager.getSortedProjects();
        for( Project project : projects ) {
            Map<ImmutablePlan, List<ImmutablePlan>> planListMap = allBranchPlans.get( project );

            if( planListMap == null ) {
                //List<TopLevelPlan> topLevelPlans = planManager.getPlansByProject( project );
                List<ImmutableTopLevelPlan> topLevelPlans = cachedPlanManager.getPlansByProject( project );
                planListMap = new LinkedHashMap<ImmutablePlan, List<ImmutablePlan>>();
                for( ImmutableTopLevelPlan topLevelPlan : topLevelPlans ) {
                    List<ImmutablePlan> planList = planListMap.get( topLevelPlan );
                    if( planList  == null ) {
                        planList = new ArrayList<ImmutablePlan>();
                        List<ImmutablePlan> relevantPlans = getRelevantPlansForTopLevelPlan( topLevelPlan );
                        if( relevantPlans.size() > 0 ) {
                            planList.addAll( relevantPlans );
                            planListMap.put( topLevelPlan, planList );
                        }
                    }
                }
                if( planListMap.size() > 0 ) {
                    allBranchPlans.put( project, planListMap );
                }
            }
        }
        return "success";
    }

    private List<ImmutablePlan> getRelevantPlansForTopLevelPlan( ImmutablePlan topLevelPlan ) {
        List<ImmutablePlan> chainBranchList = new ArrayList<ImmutablePlan>();
        if( StringUtils.isEmpty( currentlyFilteredBranch ) ) {
            chainBranchList.add( topLevelPlan );
        }

        List<ImmutableChainBranch> chainBranches = cachedPlanManager.getBranchesForChain(topLevelPlan);
        for( ImmutableChainBranch chainBranch : chainBranches ) {
            if(topLevelPlan.equals(chainBranch.getMaster())) {
                if( !branchNames.contains( chainBranch.getBuildName() ) ) {
                    branchNames.add( chainBranch.getBuildName() );
                }
                if( StringUtils.isNotEmpty( currentlyFilteredBranch ) ) {
                    if( chainBranch.getBuildName().equals( currentlyFilteredBranch ) ) {
                        chainBranchList.add( chainBranch );
                    }
                } else {
                    chainBranchList.add( chainBranch );
                }
            }
        }
        return chainBranchList;
    }

    @SuppressWarnings("unused")
    public Map<Project, Map<ImmutablePlan, List<ImmutablePlan>>> getAllBranchPlans() {
        return allBranchPlans;
    }

    @SuppressWarnings("unused")
    public Map<ImmutablePlan, List<ImmutablePlan>> getPlansForProject( Project project ) {
        return allBranchPlans.get( project );
    }

    @SuppressWarnings("unused")
    public boolean isFavourite( ImmutablePlan plan ) {
        return planFavouriteService.isFavourite( plan, getUser() );
    }

    @SuppressWarnings("unused")
    public List<String> getBranchNames() {
        return branchNames;
    }

    @SuppressWarnings("unused")
    public String getCurrentlyFilteredBranch() {
        return currentlyFilteredBranch;
    }

    @SuppressWarnings("unused")
    public boolean hasPlanPermission( String permission, Plan plan ) {
        Permission bambooPermission = BambooPermission.buildFromName(permission);
        return bambooPermissionManager.hasPlanPermission( bambooPermission , plan.getPlanKey() );
    }
}
